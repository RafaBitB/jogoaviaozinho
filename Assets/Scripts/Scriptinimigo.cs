﻿using UnityEngine;
using System.Collections;

public class Scriptinimigo : MonoBehaviour {

    float larguraTela;
    GameObject jogadorFelpudo;
    bool marcouPonto;

    // Use this for initialization
    void Start () {
        GetComponent<Rigidbody2D>().velocity = new Vector2(-4, 0);

        float alturaTela = Camera.main.orthographicSize;
        larguraTela = alturaTela / Screen.height * Screen.width;
        jogadorFelpudo = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
        
        if (this.transform.position.x < -larguraTela)
        {
            
            Destroy(this.gameObject);
        }
        else
        {
            if(jogadorFelpudo != null)
            {
                if (transform.position.x < jogadorFelpudo.transform.position.x)
                {
                    if (!marcouPonto)
                    {
                        marcouPonto = true;
                        GetComponent<Rigidbody2D>().isKinematic = false;
                        GetComponent<Rigidbody2D>().velocity = (new Vector2(-7.5f, 5.0f));
                        //Adiciona uma rotação no objeto no sentido anti-horário:
                        GetComponent<Rigidbody2D>().AddTorque(30f);
                        //Adiciona uma coloração vermelha ao objeto:
                        GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.45f, 0.45f);


                        //SendMessage executa uma função que está em outro scrypt.
                        jogadorFelpudo.SendMessage("MarcaPonto");
                    }
                }
            }
            
        }

	}
}
