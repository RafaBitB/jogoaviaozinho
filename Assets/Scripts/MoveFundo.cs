﻿using UnityEngine;
using System.Collections;

public class MoveFundo : MonoBehaviour {

    float larguraTela;

    // Use this for initialization
    void Start () {

        

        SpriteRenderer grafico = GetComponent<SpriteRenderer>();


        //Recebe o tamanho da borda da imagem em x
        float larguraImagem = grafico.bounds.size.x;
        //Recebe o tamanho da borda da imagem em y
        float alturaImagem = grafico.bounds.size.y;

        //Recebe o valor da distancia do meio da tela até o topo/bot x 2
        float alturaTela = Camera.main.orthographicSize * 2f;
        larguraTela = alturaTela / Screen.height * Screen.width;

        Vector2 novaEscala = transform.localScale;
        
        //Encontrar a proporção entre imagem e tela
        novaEscala.x = (larguraTela / larguraImagem) + 0.1f;
        novaEscala.y = (alturaTela / alturaImagem);

        this.transform.localScale = novaEscala;

        if(this.name == "ImagemFundoB")
        {
            transform.position = new Vector2(larguraTela, 0F);
        }

        //Move o backgound para esquerda, para dar a impressão de o personagem estar andando para a direita.
        GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0);

    }
	
	// Update is called once per frame
	void Update () {
	
        if(transform.position.x <= -larguraTela)
        {
            transform.position = new Vector2(larguraTela, 0F);
        }

	}
}
