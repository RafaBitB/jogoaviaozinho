﻿using UnityEngine;
using System.Collections;

public class GameEngine : MonoBehaviour {

    public GameObject inimigo;

	void Comecou ()
    {
        //Invoca uma função repetidas vezes(nome da funçao, delay da invoke, taxa de repetição).
        InvokeRepeating("CriaInimigo", 0, 1.5f);
	}
    void Acabou()
    {
        CancelInvoke("CriaInimigo");
    }
    void CriaInimigo()
    {
        //Random.value retorna um valor aleatorio de 0.0 a 1.0;
        // - 5 para o valor aleatório varias de - a 5.
        float alturaAleatoria = 10.0f * Random.value - 5;

        GameObject novoInimigo = Instantiate(inimigo);
        novoInimigo.transform.position = new Vector2(17.0f,alturaAleatoria);
    }
}
