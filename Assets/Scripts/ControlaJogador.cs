﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlaJogador : MonoBehaviour {

    public bool comecou;
    public GameObject particulaPena;
    public Text textoScore;
    int pontuacao;
    bool acabou;
    Rigidbody2D corpoJogador;
    Vector2 forcaImpulso = new Vector2(0,500f);
    GameObject gameEngine;

	void Start () {

        gameEngine = GameObject.FindGameObjectWithTag("MainCamera");
        corpoJogador = GetComponent<Rigidbody2D>();
        TextoInicio();
        
	}
	void Update () {
        //Se o click(esquerdo) do mouse for clicado
       if (!acabou){
            if (Input.GetButtonDown("Fire1"))
            {
                //Se o jogo não começou
                if (!comecou)
                {
                    //Começa o jogo
                    comecou = true;
                    //Ativa a gravidade sobre o corpo
                    this.corpoJogador.isKinematic = false;

                    TextoPontos();

                    gameEngine.SendMessage("Comecou");
                }

                corpoJogador.velocity = new Vector2(0, 0);
                corpoJogador.AddForce(forcaImpulso);

                //Cada Clique instancia a particulaPena na posição do jogador.
                GameObject peninhas = Instantiate(particulaPena);
                peninhas.transform.position = this.transform.position + new Vector3(0, 1.6f, 0);

            }

            
        }

        float alturaFelpudoEmPixels = Camera.main.WorldToScreenPoint(transform.position).y;
        
        if(alturaFelpudoEmPixels > Screen.height)
        {
            DanoNoJogador();
        }
        else if(alturaFelpudoEmPixels < 0)
        {
            Destroy(this.gameObject);
            FimDeJogo();
        }
        //Fazendo a rotação quando o personagem sobe e desce
                            //Unidade para rotação de objetos 2D,3D.
        transform.rotation = Quaternion.Euler(0, 0, corpoJogador.velocity.y*3f);
                
	}

    void OnCollisionEnter2D()
    {
        if (!acabou)
        {
            DanoNoJogador();
        }
    }
    
    void DanoNoJogador()
    {
        //Desabilita a colisão:
        GetComponent<Collider2D>().enabled = false;
        //Passa o valor da velocidade nos eixos X e Y para zero:
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //Adiciona um força no eixo X para que o personagem seja empurrado para esquerda:
        GetComponent<Rigidbody2D>().AddForce(new Vector2(-200f, 0));
        //Adiciona uma rotação no objeto no sentido anti-horário:
        GetComponent<Rigidbody2D>().AddTorque(300f);
        //Adiciona uma coloração vermelha ao objeto:
        GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.45f, 0.45f);
        acabou = true;
    }
    void TextoInicio()
    {
        textoScore.transform.position = new Vector2(Screen.width / 2, Screen.height - 200);
        textoScore.text = "Toque para iniciar!";
        textoScore.fontSize = 35;
    }
    void TextoPontos()
    {
        textoScore.transform.position = new Vector2(Screen.width/2, Screen.height - 200);
        textoScore.text = pontuacao.ToString();
        textoScore.fontSize = 55;
    }
    void MarcaPonto()
    {
        pontuacao += 10;
        textoScore.text = pontuacao.ToString();
    }
    void FimDeJogo()
    {
        gameEngine.SendMessage("Acabou");
        RecarregarCena();
        //Invoke("RecarregarCena",1.0f);
    }
    void RecarregarCena()
    {
       //Application.LoadLevel("Cena01");
       UnityEngine.SceneManagement.SceneManager.LoadScene("Cena01");
    }
}
